Copy Both files and place in some folder
Open terminal and move to the folder where scripts are present.

1. SplitFiles.py


SplitFiles.py ENTER PATH WHERE BULK FILES ARE PRESENT

Eg:

If Bulk files are present under C:\Workspace\rawfiles then 

SplitFiles.py C:\Workspace\rawfiles

Press Enter

SplitFiles Folder will be created under C:\Workspace\rawfiles and split files will be created under that.

Process the split files through Extension

2. merge.py

SplitFiles.py ENTER PATH WHERE DOWNLOADED CSV FILES PRESENT

Eg:

If Bulk files are present under C:\Workspace\converted then 

SplitFiles.py C:\Workspace\converted

Press Enter

mergedCSVFiles Folder will be created under C:\Workspace\converted and split files will be created under that.


NOTE: Both SplitFiles & mergedCSVFiles will be cleared everytime the script runs

