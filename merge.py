import os
import sys
import csv

import time

import glob
import shutil
import pandas as pd

list_result = []
fileList = os.listdir(sys.argv[1])

path = os.path.join(sys.argv[1], "mergedCSVFiles") 
try: 
    os.mkdir(path) 
except OSError as error: 
    shutil.rmtree(path)
    os.mkdir(path) 


for x in os.listdir(sys.argv[1]):
    if x.endswith(".csv"):
        list_result.append(x.partition("_split")[0])
    
set_result = (set(list_result))

for item in set_result:
    files = glob.glob(sys.argv[1]+"\\"+item+"_split*.csv")
  
    df = pd.concat(map(pd.read_csv, files))
 
    dataFrame = pd.DataFrame(df)
    dataFrame.to_csv(path+"\\"+item+".csv")
    
    rows = []
    print(item)
    with open(path+"\\"+item+".csv", newline='') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            rows.append(row)
      
    rows[0] = ['Keyword','Name','Address','PriceRange','Phone','Website','Latitude','Longitude','Stars','BusinessCategory','Description','Description2','ReviewsCount','ServiceOptions1','ServiceOptions2','ServiceOptions3','ServiceOptions4','ServiceOptions5','Photo1','Photo2','Photo3','Photo4','Photo5','Photo6','Photo7','Photo8','Photo9','Photo10','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','ReviewSummary1','ReviewSummary2','ReviewSummary3','ReviewSummary4','ReviewSummary5','ReviewSummary6','ReviewSummary7','ReviewSummary8','ReviewSummary9','ReviewSummary10','5stars','4stars','3stars','2stars','1stars','FullReview1Name','FullReview1Text','FullReview1Photo','FullReview2Name','FullReview2Text','FullReview2Photo','FullReview3Name','FullReview3Text','FullReview3Photo','FullReview4Name','FullReview4Text','FullReview4Photo','FullReview5Name','FullReview5Text','FullReview5Photo','FullReview6Name','FullReview6Text','FullReview6Photo','FullReview7Name','FullReview7Text','FullReview7Photo','FullReview8Name','FullReview8Text','FullReview8Photo','FullReview9Name','FullReview9Text','FullReview9Photo','FullReview10Name','FullReview10','TextFullReview10Photo']
    
    with open(path+"\\"+item+".csv", 'w', newline='') as csvfile:
       writer = csv.writer(csvfile)
       writer.writerows(rows)