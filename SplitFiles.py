
# import OS
import os
import sys
import shutil

splitLen = 100        
outputBase = 'split'

path = os.path.join(sys.argv[1], "splitFiles") 
try: 
    os.mkdir(path) 
except OSError as error: 
    shutil.rmtree(path)
    os.mkdir(path) 
 
for x in os.listdir(sys.argv[1]):
    if x.endswith(".txt"):
        # Prints only text file present in My Folder
        input = open(sys.argv[1]+'\\'+x, 'r').read().split('\n')

        at = 1
        for lines in range(0, len(input), splitLen):
            # First, get the list slice
            outputData = input[lines:lines+splitLen]
            
            base=os.path.basename(x)
            inputfile = os.path.splitext(base)[0]

            # Now open the output file, join the new slice with newlines
            # and write it out. Then close the file.
            output = open(path+"\\"+inputfile+'_'+outputBase + str(at) + '.txt', 'w')
            output.write('\n'.join(outputData))
            output.close()

            # Increment the counter
            at += 1
        
